/**
 * Created by omfg on 01.12.2016.
 */
public class Main {
    //Поток системная единица исполнения кода, которая выполняет к-л задачу

    public static long number;
    public static void main(String[] args) {
        Thread t1 = new Thread(new Worker());
        Thread t2 = new Thread(new Worker());
        Thread t3 = new Thread(new Worker());
        Thread t4 = new Thread(new Worker());
        t1.start();
        t2.start();
        t3.start();
        t4.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(number);

    }
    public synchronized static void increaceNumber(){
        number++;
    }


}
